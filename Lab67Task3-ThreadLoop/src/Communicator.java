import java.util.Scanner;

/**
 *
 * @author thomasdwidinata
 */
public class Communicator {
    
    private String data = new String(); // Declare a global variable
    private Object shared = new Object(); // Declare a flag for Syncronisation usage, this will be used to notify the threads
    
    public class MyScanner implements Runnable {
        private Scanner scan;       
        public MyScanner()
        {
            scan = new Scanner(System.in); // Creating new instance of Scanner
        }

		@Override
        public void run() { // The Runnable interface requires implementation of run() function which will be executed when the thread has started
        	try {
        		while(true) // Will always run
	        	{
        			System.out.printf("MYSCANNER: Enter a text: "); // Notify the user that the Scanner ready for input
	                data = scan.nextLine(); // wait for user input
        			synchronized(shared){ // Syncronised uses a dummy variable to check whether other threads that using the `shared` are notified when the global variable changed, this method is more efficient in order to notify threads for data sharing and safer
    	                shared.notifyAll(); // Notify all threads that the input has changed
    	                if(data.equals("x")) { // If the data is "x" , terminate thread (Exit)
    	                	return;
    	                }
            		}
	        	}
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
    }
    
    public class MyPrinter implements Runnable{
    
        private String local; // Declare local variable for saving String that received from the Main Thread

        public MyPrinter(String myData)
        {
            this.local = myData; // Saves the String from the Main Thread to local variable
        }

        @Override
        public void run() { 
        	while(true)
        	{
        		try {
        			synchronized (shared) {
        				shared.wait(); // Wait until there are notification from the other threads that the variable has been 'unlocked'
        				System.out.printf("MYPRINTER: Got a string : `%s`\n",local); // Print the variable from main thread
        	            System.out.printf("MYPRINTER: The global is : `%s`\n",data); // Print the global variable
        	            if(data.equals("x")) // If the string from global variable is "x", interrupt the Thread
        	            	return;
    				}
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
        	}
        }
    }
    
}
