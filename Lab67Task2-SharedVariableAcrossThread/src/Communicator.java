import java.util.Scanner;

/**
 *
 * @author thomasdwidinata
 */
public class Communicator {
    
    private String data; // Global variable for sharing data between threads
    private Object shared = new Object();
    
    public class MyScanner implements Runnable { // Scanner Thread
        private Scanner scan;

        public MyScanner()
        {
            scan = new Scanner(System.in); // Declaring scanner and creating new instance of Scanner
        }

        @Override
        public void run() { // Run must be implemented because of Runnable intreface enforce the use of Run() function. It will be used for the background function
            	System.out.printf("MYSCANNER: Enter a text: ");
                data = scan.nextLine();
            synchronized (shared) {
                shared.notifyAll();
			}
        	
            
        }
    }
    
    public class MyPrinter implements Runnable{
    
        private String local; // Declaring local variable for saving String that is passed from the Main Thread

        public MyPrinter(String myData)
        {
            this.local = myData; // Saving the string that received from the Main Thread
        }

        @Override
        public void run() {
        	try {
        		synchronized(shared)
        		{
        			shared.wait();
        			System.out.printf("MYPRINTER: Got a string : `%s`\n",local); // Print the local variable (From Main Thread)
    	            System.out.printf("MYPRINTER: The global is : `%s`\n",data); // Print the global variable (got from Scanner)
        		}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    }
    
}
