# Network Progamming (COSC 1179)

## Lab Week 6-7 Exercise

### Description
The purpose of this exercise is to familiarise students with concurrency programming, Threads. The task is to create a thread and share a data between multiple threads.

#### First Task (Thread Basics)
Create a thread that will print a string that is received from the Scanner on the Main Thread. The Main Thread will create a new Thread and start it. The new thread will only print the string typed by the user.

#### Second Task (Data Sharing between Threads)
The program will create 2 threads, scanner and printer. The scanner thread will try to wait for user input and assign it into global variable. The printer thread will wait until the global variable has changed.

#### Third Task (Thread Cooperation)
Almost the same as Task 2, but make it always read the user input instead of terminating after the first input.
