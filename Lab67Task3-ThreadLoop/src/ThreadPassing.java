import java.util.Scanner;

/**
 *
 * Data Sharing between Threads (Loop)
 * 
 * The task is almost the same as Task 2, but implements looping to always
 * accept input after the data has been printed
 *
 * @author thomasdwidinata
 */
public class ThreadPassing {

    public static void main(String[] args) {
    	Scanner firstScan = new Scanner(System.in);
    	String toBePassed = new String();
    	System.out.print("Enter the initial value that is going to be passed into MyPrinter Thread : ");
    	toBePassed = firstScan.nextLine();
    	Communicator comm = new Communicator(); // Creating new instance of Communicator
        Thread scanner = new Thread(comm.new MyScanner()); // Creating new Scanner Thread
        Thread printer = new Thread(comm.new MyPrinter(toBePassed)); // Creating new Printer Thread
        printer.setPriority(Thread.MAX_PRIORITY); // Set the printer priority to high
        try{
            scanner.start(); // Start the scanner thread
            printer.start(); // Start the printer thread. This printer thread will wait for the scanner to finish accepting string from user
        } catch(Exception e){
            e.printStackTrace();
        }
        
        System.out.println("Main has just exited");
    }
    
}
