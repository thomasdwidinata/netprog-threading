/**
 *
 * Printer Thread
 *
 * The purpose of this Class is to print the received String from the Constructor.
 * Because it will run on separate thread, it requires implementation of Runnable
 * The Runnable will make the object alive and do the task besides the Main Thread.
 *
 * @author thomasdwidinata
 */
public class MyThread implements Runnable {

    private String text; // Declaring local variable with type String
    
    public MyThread(String text){ // Receices a string from the caller
        this.text = text; // And assign it to the local variable
    }
    
    @Override
    public void run() { // The Runnable interface requires implementation of run() function which will be executed when the thread has started
        System.out.printf("\n*****\nHi, this is thread object number `%s` with content: `%s`\n*****\n\n", this.toString() ,text); // Just printing the received String
    }
}
