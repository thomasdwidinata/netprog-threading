import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Task 1 - Simple Threading
 * 
 * The aim of this task is to familiarise with Threads.
 * The main code will continues to scan while the thread will print the text
 * 
 * In order to make the console look better, the Thread Priority is set High
 * so that the printer can print the output first.
 *
 * @author thomasdwidinata
 */
public class JThread {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); // Declaring scanner for input method
        String text = new String(); // Declaring a variable for storing the data
        Thread myThread = null; // Create a thread variable for running the printer thread
        
        while(true) // Always ask for the input, except when user input a string "exit", and it will exit the program
        {
            System.out.printf("Enter a text : "); // Notify the user that the program is ready to receive the input 
            text = scan.nextLine(); // Wait for user input
            
            if(text.equals("exit")) // Check whether the input is "exit", if yes, exit the program
                break;
            else { // If not, start a new thread and give the string to the object
            	System.out.println("Starting new Thread...");
                myThread = new Thread( new MyThread(text) ); // Creating new Thread from new Object
                myThread.setPriority(Thread.MAX_PRIORITY); // Make the thread at the highest priority
                myThread.start(); // Start the Thread
            }
            
            try{
            	System.out.println("Main Code sleeping for 50ms"); // Notify the User that the thread are sleeping
                Thread.sleep(50); // Optional, the main thread is slept for 50ms in order to make the printer print the string first rather than the main asking for input
            } catch (InterruptedException ex) { // Catch whether the thread interrupted in the middle of the sleep
                Logger.getLogger(JThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Main Code woke up!"); // Notify that the main thread has woke up after Thread.sleep()
        }
        scan.close(); // Safely close the Scanner
        System.out.println("Bye"); // Notify user that the program will exit
    }
    
}
