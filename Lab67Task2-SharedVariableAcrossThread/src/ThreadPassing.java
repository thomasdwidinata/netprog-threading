import java.util.Scanner;

/**
 *
 * Data Sharing between Thread
 * 
 * The purpose of this task is to share data between multiple threads. There are
 * Scanner thread and Printer Thread.
 *
 * @author thomasdwidinata
 */
public class ThreadPassing {
	
    public static void main(String[] args) {
    	Scanner firstScan = new Scanner(System.in);
    	String toBePassed = new String();
    	System.out.print("Enter the initial value that is going to be passed into MyPrinter Thread : ");
    	toBePassed = firstScan.nextLine();
    	firstScan.close();
        Communicator comm = new Communicator(); // Create new instance of Communicator Object containing MyScanner and MyPrinter
        Thread scan = new Thread(comm.new MyScanner()); // Creating new Object Thread comm's MyScanner 
        Thread print = new Thread(comm.new MyPrinter(toBePassed)); // Creating new Object Thread comm's MyPrinter, and also pass a string from the Main Thread
        try{
            scan.start(); // Start the thread
            print.start(); // Start printer thread
        } catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
}
